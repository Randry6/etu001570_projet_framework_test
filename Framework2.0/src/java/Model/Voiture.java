/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import utilities.Annotation;
import utilities.Model_View;

/**
 *
 * @author Randry
 */
public class Voiture {
    private int nombre;
    private String nom;

    public int getNombre() {
        return nombre;
    }

    public void setNombre(int nombre) {
        this.nombre = nombre;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Voiture() {
    }
    
    
    
    @Annotation(url ="voiture.do")
    public Model_View get(){
        Model_View v = new Model_View();
        v.setAttribute("v", 2);
        v.setAttribute("string", "string");
        v.setJsp("Home.jsp");
        return v;
    }
    
    @Annotation(url="vehicule.do")
    public Model_View insert(){
        Model_View v = new Model_View();
        v.setAttribute("v", this.nombre);
        v.setAttribute("string", this.nom);
        v.setJsp("Home.jsp");
        return v;
        
    }
    
}
